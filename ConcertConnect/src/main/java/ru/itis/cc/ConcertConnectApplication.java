package ru.itis.cc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcertConnectApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConcertConnectApplication.class, args);
    }

}
