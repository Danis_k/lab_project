package ru.itis.cc.dto.song;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.dto.singer.SingerDto;
import ru.itis.cc.model.Singer;
import ru.itis.cc.model.Song;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Песня")
public class SongDto {
    @Schema(description = "идентификатор песни", example = "1")
    private Long id;
    @Schema(description = "название песни", example = "Пьяный дождь")
    private String nameOfSong;
    @Schema(description = "текст песни", example = "ля-ля-ля")
    private String textOfSong;
    @Schema(description = "жанр песни", example = "поп")
    private String genre;

    public static SongDto from(Song song){
        return SongDto.builder()
                .id(song.getId())
                .nameOfSong(song.getNameOfSong())
                .textOfSong(song.getTextOfSong())
                .genre(song.getGenre())
                .build();
    }

    public static List<SongDto> from(List<Song> songs){
        return songs.stream().map(SongDto::from).collect(Collectors.toList());
    }
}
