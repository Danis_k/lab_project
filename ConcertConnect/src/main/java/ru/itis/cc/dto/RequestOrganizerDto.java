package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestOrganizerDto {
    @NotEmpty(message = "не может быть пустым")
    private String nameCompany;
}
