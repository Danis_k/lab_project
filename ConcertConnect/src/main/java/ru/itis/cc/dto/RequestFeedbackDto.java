package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.validation.constaraint.NotSameValues;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@NotSameValues(fields = {"name", "feedbackText"})
public class RequestFeedbackDto {

    @NotBlank(message = "the name should not be empty or null")
    private String name;

    @Size(min = 1, max = 200, message = "{newFeedback.description.size}")
    private String feedbackText;

    @NotNull
    private Long clientId;
    @NotNull
    private Long concertId;

}
