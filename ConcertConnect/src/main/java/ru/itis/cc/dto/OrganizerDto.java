package ru.itis.cc.dto;

import lombok.Builder;
import lombok.Data;
import ru.itis.cc.model.Organizer;

import java.util.List;
import java.util.stream.Collectors;
@Data
@Builder
public class OrganizerDto {
    private String nameCompany;

    private Long userId;

    public static OrganizerDto fromEntity(Organizer organizer) {
        return OrganizerDto.builder()
                .nameCompany(organizer.getNameCompany())
                .userId(organizer.getUser().getId())
                .build();
    }

    public static List<OrganizerDto> fromEntity (List<Organizer> organizers) {
        return organizers.stream()
                .map(OrganizerDto::fromEntity)
                .collect(Collectors.toList());
    }
}
