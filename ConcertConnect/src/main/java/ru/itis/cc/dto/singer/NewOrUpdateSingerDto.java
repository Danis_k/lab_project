package ru.itis.cc.dto.singer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Новый исполнитель")
public class NewOrUpdateSingerDto {

    @Schema(description = "имя исполнителя", example = "Макс")
    private String firstName;
    @Schema(description = "фамилия исполнителя", example = "Корж")
    private String lastName;
    @Schema(description = "ссылка на социальные сети", example = "https://www.youtube.com/@maxkorzhmus")
    private String socialMedia;

}
