package ru.itis.cc.dto.singer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.dto.concert.ConcertDto;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Singer;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Исполнитель")
public class SingerDto {
    @Schema(description = "идентификатор исполнителя", example = "1")
    private Long id;
    @Schema(description = "имя исполнителя", example = "Макс")
    private String firstName;
    @Schema(description = "фамилия исполнителя", example = "Корж")
    private String lastName;
    @Schema(description = "ссылка на социальные сети", example = "https://www.youtube.com/@maxkorzhmus")
    private String socialMedia;

    public static SingerDto from(Singer singer){
        return SingerDto.builder()
                .id(singer.getId())
                .firstName(singer.getFirstName())
                .lastName(singer.getLastName())
                .socialMedia(singer.getSocialMedia())
                .build();
    }

    public static List<SingerDto> from(List<Singer> singers){
        return singers.stream().map(SingerDto::from).collect(Collectors.toList());
    }
}
