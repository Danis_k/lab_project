package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Client;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestClientDto {
    @NotEmpty(message = "Имя не может быть пустым или содержать только пробельные символы")
    private String firstName;

    @NotEmpty(message = "фамилия не может быть пустым")
    private String lastName;

    @Pattern(regexp = "\\d{3}-\\d{3}-\\d{4}", message = "Номер телефона должен иметь формат XXX-XXX-XXXX")
    private String phoneNumber;

    public static RequestClientDto fromEntity(Client client) {
        return RequestClientDto.builder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .phoneNumber(client.getPhoneNumber())
                .build();
    }

    public static List<ClientDto> fromEntity (List<Client> client) {
        return client.stream()
                .map(ClientDto::fromEntity)
                .collect(Collectors.toList());
    }
}
