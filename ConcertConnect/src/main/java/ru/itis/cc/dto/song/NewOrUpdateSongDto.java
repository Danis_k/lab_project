package ru.itis.cc.dto.song;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Новая песня")
public class NewOrUpdateSongDto {
    @Schema(description = "название песни", example = "Пьяный дождь")
    private String nameOfSong;
    @Schema(description = "текст песни", example = "ля-ля-ля")
    private String textOfSong;
    @Schema(description = "жанр песни", example = "поп")
    private String genre;

    @Schema(description = "идентификаторы исполнителей", example = "1,2")
    private List<Long> singersId;
}
