package ru.itis.cc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "user response")
public class UserResponseDto {
    private Long id;

    private String email;

    private User.State state;

    public static UserResponseDto from(User user) {
        return UserResponseDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .state(user.getState())
                .build();
    }
}
