package ru.itis.cc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "create user request")
public class CreateUserRequestDto {
    @Email
    private String email;

    @Size(min = 8, max = 63, message = "Password should contains from 8 to 63 symbols")
    @NotBlank
    private String password;
}
