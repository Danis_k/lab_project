package ru.itis.cc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "update user request")
public class UpdateUserRequestDto {
    @Email
    private String email;
    @Size(min = 8, max = 63, message = "Password should contains from 8 to 63 symbols")
    private String password;
}
