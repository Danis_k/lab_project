package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FeedbackPage {
    private List<FeedbackDto> feedbacks;

    private Integer totalPagesCount;
}
