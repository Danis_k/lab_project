package ru.itis.cc.dto.concert;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Concert;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Концерт")
public class ConcertDto {

    @Schema(description = "идентификатор концерта", example = "1")
    private Long id;
    @Schema(description = "город проведения концерта", example = "Казань")
    private String city;
    @Schema(description = "дата концерта", example = "2023-09-11")
    private Date data;
    @Schema(description = "место проведения концерта", example = "Корстон")
    private String place;

    //ToDo:добавить потом
//    @Schema(description = "идентификатор организатора концерта", example = "1")
//    private Long organizerID;

    @Schema(description = "идентификатор исполнителя концерта", example = "1")
    private Long singerID;

    public static ConcertDto from(Concert concert){
        return ConcertDto.builder()
                .id(concert.getId())
                .city(concert.getCity())
                .data(concert.getData())
                .place(concert.getPlace())
                .singerID(concert.getSinger().getId())
                .build();
    }

    public static List<ConcertDto> from(List<Concert> concerts){
        return concerts.stream().map(ConcertDto::from).collect(Collectors.toList());
    }
}
