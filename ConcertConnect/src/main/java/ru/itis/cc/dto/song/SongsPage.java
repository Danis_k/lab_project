package ru.itis.cc.dto.song;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.dto.concert.ConcertDto;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Страница со всеми песнями и общим количеством страниц")
public class SongsPage {
    @Schema(description = "Список песен")
    private List<SongDto> songs;

    @Schema(description = "Общее количество страниц", example = "5")
    private Integer totalPagesCount;
}
