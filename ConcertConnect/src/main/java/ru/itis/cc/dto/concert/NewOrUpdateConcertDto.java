package ru.itis.cc.dto.concert;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Новый концерт")
public class NewOrUpdateConcertDto {
    @Schema(description = "дата концерта", example = "2023-09-11")
    private Date data;
    @Schema(description = "место проведения концерта", example = "Корстон")
    private String place;

    @Schema(description = "город проведения концерта", example = "Казань")
    private String city;

    @Schema(description = "идентификатор исполнителя концерта", example = "1")
    private Long singerID;

    //ToDo: добавить потом
//    @Schema(description = "идентификатор организатора концерта", example = "1")
//    private Long organizerID;
}
