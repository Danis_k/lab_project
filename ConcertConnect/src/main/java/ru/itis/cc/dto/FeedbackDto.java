package ru.itis.cc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Feedback;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Schema(description = "feedback")
public class FeedbackDto {

    private Long id;

    private String name;

    private String feedbackText;

    private Long clientId;

    private Long concertId;

    public static FeedbackDto from(Feedback feedback) {
        return FeedbackDto.builder()
                .id(feedback.getId())
                .name(feedback.getName())
                .feedbackText(feedback.getFeedbackText())
                .clientId(feedback.getClient().getId())
                .concertId(feedback.getConcert().getId())
                .build();
    }

    public static List<FeedbackDto> from (List<Feedback> feedbacks) {
        return feedbacks.stream()
                .map(FeedbackDto::from)
                .collect(Collectors.toList());
    }
}
