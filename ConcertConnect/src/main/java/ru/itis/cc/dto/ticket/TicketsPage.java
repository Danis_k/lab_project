package ru.itis.cc.dto.ticket;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "all tickets page")
public class TicketsPage {
    @Schema(description = "all tickets")
    private List<TicketDto> tickets;

    @Schema(description = "total pages count", example = "3")
    private Integer totalPagesCount;
}
