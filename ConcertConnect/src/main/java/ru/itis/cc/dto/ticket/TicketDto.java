package ru.itis.cc.dto.ticket;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Ticket;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "ticket")
public class TicketDto {
    private Long id;
    private String location;
    private String place;
    private Long clientId;
    private Long concertId;

    public static TicketDto from(Ticket ticket) {
        return TicketDto.builder()
                .id(ticket.getId())
                .place(ticket.getPlace())
                .location(ticket.getLocation())
                .clientId(ticket.getClient().getId())
                .concertId(ticket.getConcert().getId())
                .build();
    }

    public static List<TicketDto> from (List<Ticket> tickets) {
        return tickets.stream()
                .map(TicketDto::from)
                .collect(Collectors.toList());
    }
}



