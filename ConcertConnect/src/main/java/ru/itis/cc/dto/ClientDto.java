package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Client;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientDto {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    public static ClientDto fromEntity(Client client) {
        return ClientDto.builder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .phoneNumber(client.getPhoneNumber())
                .build();
    }

    public static List<ClientDto> fromEntity (List<Client> client) {
        return client.stream()
                .map(ClientDto::fromEntity)
                .collect(Collectors.toList());
    }
}
