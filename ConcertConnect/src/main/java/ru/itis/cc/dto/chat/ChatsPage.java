package ru.itis.cc.dto.chat;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "all chats page")
public class ChatsPage {
    @Schema(description = "all chats")
    private List<ChatDto> chats;

    @Schema(description = "total pages count", example = "3")
    private Integer totalPagesCount;
}
