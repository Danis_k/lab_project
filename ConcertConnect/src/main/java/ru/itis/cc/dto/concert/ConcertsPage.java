package ru.itis.cc.dto.concert;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Страница с концертами и общим количеством страниц")
public class ConcertsPage {
    @Schema(description = "Список концертов")
    private List<ConcertDto> concerts;

    @Schema(description = "Общее количество страниц", example = "5")
    private Integer totalPagesCount;

}
