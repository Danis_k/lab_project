package ru.itis.cc.dto.message;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "all messages page")
public class MessagesPage {
    @Schema(description = "all messages")
    private List<MessageDto> messages;

    @Schema(description = "total pages count", example = "3")
    private Integer totalPagesCount;
}

