package ru.itis.cc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.User;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

    private String email;

    private User.State state;

    public static UserDto fromEntity(User user) {
        return UserDto.builder()
                .email(user.getEmail())
                .state(user.getState())
                .build();
    }

    public static UserDto fromEntity(CreateUserRequestDto user) {
        return UserDto.builder()
                .email(user.getEmail())
                .build();
    }

}
