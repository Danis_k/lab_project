package ru.itis.cc.dto.singer;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.dto.concert.ConcertDto;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Страница со всеми исполнителями и общим количеством страниц")
public class SingersPage {
    @Schema(description = "Список исполнителей")
    private List<SingerDto> singers;

    @Schema(description = "Общее количество страниц", example = "5")
    private Integer totalPagesCount;
}
