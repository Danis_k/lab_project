package ru.itis.cc.dto.message;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.cc.model.Message;


import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "message")
public class MessageDto {
    private Long id;
    private String textOfMessage;
    private Long clientId;
    private Long chatId;

    public static MessageDto from(Message message) {
        return MessageDto.builder()
                .id(message.getId())
                .textOfMessage(message.getTextOfMessage())
                .clientId(message.getClient().getId())
                .chatId(message.getChat().getId())
                .build();
    }

    public static List<MessageDto> from(List<Message> messages) {
        return messages.stream()
                .map(MessageDto::from)
                .collect(Collectors.toList());
    }

}
