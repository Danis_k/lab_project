package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ClientDto;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.RequestClientDto;

import javax.validation.Valid;

@Tags(
        value = {
                @Tag(name = "clients")
        }
)
@RequestMapping("/clients")
public interface ClientApi {

    @Operation(summary = "create a client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created client",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ClientDto.class))
                    })
    })
    @PostMapping("/create")
    ResponseEntity<ClientDto> createClient(@Valid @RequestBody RequestClientDto clientDto);

    @Operation(summary = "get client by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "client",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ClientDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{client-id}")
    ResponseEntity<ClientDto> getClientById(@Parameter(description = "client ID") @PathVariable("client-id") Long clientId);

    @Operation(summary = "update a client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "updated client",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ClientDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/update/{client-id}")
    ResponseEntity<ClientDto> updateClient(@Parameter(description = "client ID") @PathVariable("client-id") Long clientId,
                                           @Valid @RequestBody RequestClientDto clientDto);
}
