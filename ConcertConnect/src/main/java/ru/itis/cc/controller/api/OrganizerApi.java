package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.OrganizerDto;
import ru.itis.cc.dto.RequestOrganizerDto;

import javax.validation.Valid;

@Tags(
        value = {
                @Tag(name = "organizers")
        }
)
@RequestMapping("/organizers")
public interface OrganizerApi {

    @Operation(summary = "create an organizer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created organizer",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = OrganizerDto.class))
                    })
    })
    @PostMapping("/create")
    ResponseEntity<OrganizerDto> createOrganizer(@Valid @RequestBody RequestOrganizerDto organizerDto);

    @Operation(summary = "get organizer by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "organizer",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = OrganizerDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{organizer-id}")
    ResponseEntity<OrganizerDto> getOrganizerById(@Parameter(description = "organizer ID") @PathVariable("organizer-id") Long organizerId);

    @Operation(summary = "update an organizer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "updated organizer",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = OrganizerDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/update/{organizer-id}")
    ResponseEntity<OrganizerDto> updateOrganizer(@Parameter(description = "organizer ID") @PathVariable("organizer-id") Long organizerId,
                                                 @Valid @RequestBody RequestOrganizerDto organizerDto);
}
