package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.ticket.TicketDto;
import ru.itis.cc.dto.ticket.TicketsPage;

@Tags(
        value = {
                @Tag(name = "tickets")
        }
)
@RequestMapping("/tickets")
public interface TicketApi {

    @Operation(summary = "get ticket by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "ticket",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = TicketDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{ticket-id}")
    ResponseEntity<TicketDto> getTicketById(@Parameter(description = "ticket ID", example = "1") @PathVariable("ticket-id") Long id);

    @Operation(summary = "delete ticket")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "ticket deleted"),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping("/{ticket-id}")
    ResponseEntity<TicketDto> deleteTicket(
            @Parameter(description = "ticket id", example = "1") @PathVariable("ticket-id") Long id);

    @Operation(summary = "create ticket")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "new ticket",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = TicketDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{ticket-id}")
    ResponseEntity<TicketDto> createTicket(
            @Parameter(description = "ticket ID", example = "1") @PathVariable("ticket-id") TicketDto ticketDto);

    @Operation(summary = "get all tickets")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "all tickets",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = TicketsPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<TicketsPage> getAllTickets(@Parameter(description = "page number") @RequestParam("page") int page);

}






