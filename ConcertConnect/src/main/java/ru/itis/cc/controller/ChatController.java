package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.ChatApi;
import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.chat.ChatsPage;
import ru.itis.cc.service.ChatService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RequiredArgsConstructor
@RestController
@RequestMapping("/chats")
public class ChatController implements ChatApi {
    private final ChatService chatService;


    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "all chats",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ChatsPage.class))
                    })
    })
    @Override
    public ResponseEntity<ChatsPage> getAllChats(int page) {
        return ResponseEntity.ok(chatService.getAllChats(page));
    }

    @Override
    public ResponseEntity<ChatDto> getChatById(Long id) {
        return ResponseEntity.ok(chatService.getChatById(id));
    }


    @Override
    public ResponseEntity<ChatDto> deleteChat(Long id) {
        chatService.deleteChat(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<ChatDto> createChat(ChatDto chatDto) {
        return ResponseEntity.accepted().body(chatService.createChat(chatDto));
    }
}

