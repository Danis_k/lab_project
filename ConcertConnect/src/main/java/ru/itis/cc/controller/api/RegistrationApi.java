package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.itis.cc.dto.CreateUserRequestDto;
import ru.itis.cc.dto.UserResponseDto;

import javax.validation.Valid;

@Tags(
        value = {
                @Tag(name = "registration")
        }
)
public interface RegistrationApi {
        @Operation(summary = "user registration")
        @ApiResponses(value = {
                @ApiResponse(responseCode = "201", description = "User account successfully created.",
                        content = {
                                @Content(mediaType = "application/json", schema = @Schema(implementation = CreateUserRequestDto.class))
                        })
        })
        @PostMapping("/sign_up")
        public ResponseEntity<UserResponseDto> signUp(@Valid @RequestBody CreateUserRequestDto createUserDto);
}
