package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.cc.controller.api.ConcertApi;
import ru.itis.cc.dto.concert.ConcertDto;
import ru.itis.cc.dto.concert.ConcertsPage;
import ru.itis.cc.dto.concert.NewOrUpdateConcertDto;
import ru.itis.cc.service.ConcertService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/concerts")
public class ConcertController implements ConcertApi {
    private final ConcertService concertService;

    @Override
    public ResponseEntity<ConcertsPage> getAllConcerts(int page){
        return ResponseEntity.ok(concertService.getAllConcerts(page));
    }

    @Override
    public ResponseEntity<ConcertDto> addConcert(NewOrUpdateConcertDto newOrUpdateConcertDto){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(concertService.addConcert(newOrUpdateConcertDto));
    }

    @Override
    public ResponseEntity<ConcertDto> getConcert(Long id) {
        return ResponseEntity.ok(concertService.getConcert(id));
    }

    @Override
    public ResponseEntity<ConcertDto> updateConcert(Long id, NewOrUpdateConcertDto updatedConcert) {
        return ResponseEntity.accepted().body(concertService.updateConcert(id, updatedConcert));
    }

    @Override
    public ResponseEntity<?> deleteConcert(Long id) {
        concertService.deleteConcert(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<ConcertDto> publishConcert(Long id) {
        return ResponseEntity.accepted().body(concertService.publishConcert(id));
    }
}
