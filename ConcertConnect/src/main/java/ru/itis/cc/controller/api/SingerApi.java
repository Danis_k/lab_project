package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.singer.NewOrUpdateSingerDto;
import ru.itis.cc.dto.singer.SingerDto;
import ru.itis.cc.dto.singer.SingersPage;
import ru.itis.cc.dto.song.SongsPage;

@Tags(value = {
        @Tag(name = "singers")
})
@RequestMapping("/singers")
public interface SingerApi {
    @Operation(summary = "Получение списка всех исполнителей")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с исполнителями",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingersPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<SingersPage> getAllSingers(@Parameter(description = "Номер страницы") @RequestParam("page") int page);

    @Operation(summary = "Получение списка всех исполнителей")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с исполнителями",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingersPage.class))
                    })
    })
    @GetMapping("/{singer-id}/songs")
    ResponseEntity<SongsPage> getAllSongsBySinger(@Parameter(description = "Номер страницы") @RequestParam("page") int page,
                                                  @PathVariable("singer-id") Long id);

    @Operation(summary = "Добавление нового исполнителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавление исполнителя",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingerDto.class))
                    })
    })
    @PostMapping
    ResponseEntity<SingerDto> addSinger(@RequestBody NewOrUpdateSingerDto singerDto);

    @Operation(summary = "Получение исполнителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с информацией о исполнителе",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingerDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })

    @GetMapping("/{singer-id}")
    ResponseEntity<SingerDto> getSinger(@Parameter(description = "Идентификатор исполнителя", example = "1")
                                          @PathVariable("singer-id") Long id);


    @Operation(summary = "Обновление исполнителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Обновленный исполнитель",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingerDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{singer-id}")
    ResponseEntity<SingerDto> updateSinger(
            @Parameter(description = "Идентификатор исполнителя", example = "1") @PathVariable("singer-id") Long id,
            @RequestBody NewOrUpdateSingerDto singerDto);


    @Operation(summary = "Удаление исполнителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Исполнитель удален"),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping ("/{singer-id}")
    ResponseEntity<?> deleteSinger(
            @Parameter(description = "Идентификатор исполнителя", example = "1") @PathVariable("singer-id") Long id);


    @Operation(summary = "Публикация исполнителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Опубликованный исполнитель",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SingerDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{singer-id}/publish")
    ResponseEntity<SingerDto> publishSinger(
            @Parameter(description = "Идентификатор исполнителя", example = "1") @PathVariable("singer-id") Long id);

}
