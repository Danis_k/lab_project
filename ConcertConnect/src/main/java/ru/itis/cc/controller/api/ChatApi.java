package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.chat.ChatsPage;

@Tags(
        value = {
                @Tag(name = "chats")
        }
)
@RequestMapping("/chats")
public interface ChatApi {

    @Operation(summary = "get chat by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "chat",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ChatDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{chat-id}")
    ResponseEntity<ChatDto> getChatById(@Parameter(description = "chat ID", example = "1") @PathVariable("chat-id") Long id);

    @Operation(summary = "delete chat")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "chat deleted"),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping("/{chat-id}")
    ResponseEntity<ChatDto> deleteChat(
            @Parameter(description = "chat id", example = "1") @PathVariable("chat-id") Long id);

    @Operation(summary = "create chat")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "new chat",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ChatDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{chat-id}")
    ResponseEntity<ChatDto> createChat(
            @Parameter(description = "chat ID", example = "1") @PathVariable("chat-id") ChatDto chatDto);


    @Operation(summary = "get all chats")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "all chats",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ChatsPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<ChatsPage> getAllChats(@Parameter(description = "page number") @RequestParam("page") int page);

}







