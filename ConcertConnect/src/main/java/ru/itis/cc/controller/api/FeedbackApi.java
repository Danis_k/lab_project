package ru.itis.cc.controller.api;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.FeedbackDto;
import ru.itis.cc.dto.FeedbackPage;
import ru.itis.cc.dto.RequestFeedbackDto;

import javax.validation.Valid;

@Tags(
        value = {
                @Tag(name = "feedback")
        }
)
@RequestMapping("/feedbacks")
public interface FeedbackApi {

    @Operation(summary = "getting a list of feedbacks")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "page with feedbacks",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = FeedbackPage.class))
                    })
    })
    @GetMapping()
    ResponseEntity<FeedbackPage> getAllFeedbacks(@RequestParam("page") int page);


    @Operation(summary = "adding a feedback")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "added feedback",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FeedbackDto.class))
                    })
    })
    @PostMapping()
    ResponseEntity<FeedbackDto> addFeedback(@Valid @RequestBody RequestFeedbackDto feedbackDto);

    @Operation(summary = "getting of feedback")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "page with feedback",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = FeedbackDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                                @Content(mediaType = "application/json",
                                schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{feedback-id}")
    ResponseEntity<FeedbackDto> getFeedback(@Parameter(description = "feedback id") @PathVariable("feedback-id") Long feedbackId);

    @Operation(summary = "update feedback")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "update feedback",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = FeedbackDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/{feedback-id}")
    ResponseEntity<FeedbackDto> updateFeedback(@Valid @PathVariable("feedback-id") Long feedbackId,
                                               @RequestBody RequestFeedbackDto feedbackDto);

    @Operation(summary = "delete feedback")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "delete feedback"),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @DeleteMapping("/{feedback-id}")
    ResponseEntity<?> deleteFeedback(@PathVariable("feedback-id") Long feedbackId);

    @Operation(summary = "publish of feedback")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "page with feedback",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = FeedbackDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/{feedback-id}/publish")
    ResponseEntity<FeedbackDto> publishFeedback(@PathVariable("feedback-id") Long feedbackId);

}

