package ru.itis.cc.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.TicketApi;
import ru.itis.cc.dto.ticket.TicketDto;
import ru.itis.cc.dto.ticket.TicketsPage;
import ru.itis.cc.service.TicketService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/tickets")
public class TicketController implements TicketApi {
    private final TicketService ticketService;


    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "all tickets",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = TicketsPage.class))
                    })
    })
    @Override
    public ResponseEntity<TicketsPage> getAllTickets(int page) {
        return ResponseEntity.ok(ticketService.getAllTickets(page));
    }

    @Override
    public ResponseEntity<TicketDto> getTicketById(Long id) {
        return ResponseEntity.ok(ticketService.getTicketById(id));
    }


    @Override
    public ResponseEntity<TicketDto> deleteTicket(Long id) {
        ticketService.deleteTicket(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<TicketDto> createTicket(TicketDto ticketDto) {
        return ResponseEntity.accepted().body(ticketService.createTicket(ticketDto));
    }
}


