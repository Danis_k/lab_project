package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.cc.controller.api.SingerApi;
import ru.itis.cc.dto.singer.NewOrUpdateSingerDto;
import ru.itis.cc.dto.singer.SingerDto;
import ru.itis.cc.dto.singer.SingersPage;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.service.SingerService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/singers")
public class SingerController implements SingerApi {

    private final SingerService singerService;

    @Override
    public ResponseEntity<SingersPage> getAllSingers(int page) {
        return ResponseEntity.ok(singerService.getAllSingers(page));
    }

    @Override
    public ResponseEntity<SongsPage> getAllSongsBySinger(int page, Long id) {
        return ResponseEntity.ok(singerService.getAllSongBySinger(page, id));
    }

    @Override
    public ResponseEntity<SingerDto> addSinger(NewOrUpdateSingerDto singerDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(singerService.addSinger(singerDto));
    }

    @Override
    public ResponseEntity<SingerDto> getSinger(Long id) {
        return ResponseEntity.ok(singerService.getSinger(id));
    }

    @Override
    public ResponseEntity<SingerDto> updateSinger(Long id, NewOrUpdateSingerDto singerDto) {
        return ResponseEntity.accepted().body(singerService.updateSinger(id, singerDto));
    }

    @Override
    public ResponseEntity<?> deleteSinger(Long id) {
        singerService.deleteSinger(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<SingerDto> publishSinger(Long id) {
        return ResponseEntity.accepted().body(singerService.publishSinger(id));
    }
}
