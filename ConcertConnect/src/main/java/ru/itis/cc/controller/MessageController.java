package ru.itis.cc.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.MessageApi;

import ru.itis.cc.dto.message.MessageDto;
import ru.itis.cc.dto.message.MessagesPage;
import ru.itis.cc.service.MessageService;
@RequiredArgsConstructor
@RestController
@RequestMapping("/messages")
public class MessageController implements MessageApi {
    private final MessageService messageService;


    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "all messages",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = MessagesPage.class))
                    })
    })
    @Override
    public ResponseEntity<MessagesPage> getAllMessages(int page) {
        return ResponseEntity.ok(messageService.getAllMessages(page));
    }

    @Override
    public ResponseEntity<MessageDto> getMessageById(Long id) {
        return ResponseEntity.ok(messageService.getMessageById(id));
    }


    @Override
    public ResponseEntity<MessageDto> deleteMessage(Long id) {
        messageService.deleteMessage(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<MessageDto> createMessage (MessageDto messageDto) {
        return ResponseEntity.accepted().body(messageService.createMessage(messageDto));
    }

}
