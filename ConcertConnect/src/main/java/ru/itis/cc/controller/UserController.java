package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.UserApi;
import ru.itis.cc.dto.UpdateUserRequestDto;
import ru.itis.cc.dto.UserDto;
import ru.itis.cc.service.UserService;

@RequiredArgsConstructor
@RestController
public class UserController implements UserApi {

    private final UserService userService;

    @Override
    public ResponseEntity<UserDto> getUserById(Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @Override
    public ResponseEntity<UserDto> getUserByEmail(String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @Override
    public ResponseEntity<UserDto> updateUser(Long id, UpdateUserRequestDto updateUserDto) {
        UserDto updatedUserDto = userService.updateUser(id, updateUserDto);
        return ResponseEntity.ok(updatedUserDto);
    }

}
