package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.song.NewOrUpdateSongDto;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;

@Tags(value = {
        @Tag(name = "songs")
})
@RequestMapping("/songs")
public interface SongApi {
    @Operation(summary = "Получение списка всех песен")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница со всеми песнями",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SongsPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<SongsPage> getAllSongs(@Parameter(description = "Номер страницы") @RequestParam("page") int page);

    @Operation(summary = "Добавление новой песни")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавление песни",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SongDto.class))
                    })
    })
    @PostMapping
    ResponseEntity<SongDto> addSong(@RequestBody NewOrUpdateSongDto songDto);

    @Operation(summary = "Получение песни")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с информацией о песне",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SongDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })

    @GetMapping("/{song-id}")
    ResponseEntity<SongDto> getSong(@Parameter(description = "Идентификатор песни", example = "1")
                                        @PathVariable("song-id") Long id);


    @Operation(summary = "Обновление песни")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Обновленная песня",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SongDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{song-id}")
    ResponseEntity<SongDto> updateSong(
            @Parameter(description = "Идентификатор песни", example = "1") @PathVariable("song-id") Long id,
            @RequestBody NewOrUpdateSongDto songDto);


    @Operation(summary = "Удаление песни")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Песня удалена"),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping ("/{song-id}")
    ResponseEntity<?> deleteSong(
            @Parameter(description = "Идентификатор песни", example = "1") @PathVariable("song-id") Long id);


    @Operation(summary = "Публикация песни")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Опубликованная песня",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = SongDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{song-id}/publish")
    ResponseEntity<SongDto> publishSong(
            @Parameter(description = "Идентификатор песни", example = "1") @PathVariable("song-id") Long id);

}
