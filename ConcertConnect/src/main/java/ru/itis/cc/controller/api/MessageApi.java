package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.message.MessageDto;
import ru.itis.cc.dto.message.MessagesPage;

@Tags(
        value = {
                @Tag(name = "messages")
        }
)
@RequestMapping("/messages")
public interface MessageApi {

    @Operation(summary = "get message by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "message",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = MessageDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{message-id}")
    ResponseEntity<MessageDto> getMessageById(@Parameter(description = "message ID", example = "1") @PathVariable("message-id") Long id);

    @Operation(summary = "delete message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "message deleted"),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping("/{message-id}")
    ResponseEntity<MessageDto> deleteMessage(
            @Parameter(description = "chat id", example = "1") @PathVariable("message-id") Long id);

    @Operation(summary = "create message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "new message",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = MessageDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{message-id}")
    ResponseEntity<MessageDto> createMessage(
            @Parameter(description = "message ID", example = "1") @PathVariable("message-id") MessageDto messageDto);


    @Operation(summary = "get all messages")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "all messages",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = MessagesPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<MessagesPage> getAllMessages(@Parameter(description = "page number") @RequestParam("page") int page);

}

