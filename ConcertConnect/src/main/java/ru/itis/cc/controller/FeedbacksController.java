package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.FeedbackApi;
import ru.itis.cc.dto.FeedbackDto;
import ru.itis.cc.dto.FeedbackPage;
import ru.itis.cc.dto.RequestFeedbackDto;
import ru.itis.cc.service.impl.FeedbackServiceImpl;

@RequiredArgsConstructor
@RestController
public class FeedbacksController implements FeedbackApi {

    private final FeedbackServiceImpl feedbackService;

    @Override
    public ResponseEntity<FeedbackPage> getAllFeedbacks(int page) {
        return ResponseEntity
                .ok(feedbackService.getAllFeedback(page));
    }

    @Override
    public ResponseEntity<FeedbackDto> addFeedback(RequestFeedbackDto feedbackDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(feedbackService.addFeedback(feedbackDto));
    }

    @Override
    public ResponseEntity<FeedbackDto> getFeedback(Long feedbackId) {
        return ResponseEntity.ok(feedbackService.getFeedback(feedbackId));
    }

    @Override
    public ResponseEntity<FeedbackDto> updateFeedback(Long feedbackId, RequestFeedbackDto feedbackDto) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(feedbackService.updateFeedback(feedbackId, feedbackDto));
    }

    @Override
    public ResponseEntity<?> deleteFeedback(Long feedbackId) {
        feedbackService.delete(feedbackId);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<FeedbackDto> publishFeedback(Long feedbackId) {
        return ResponseEntity.accepted().body(feedbackService.publishFeedback(feedbackId));
    }
}

