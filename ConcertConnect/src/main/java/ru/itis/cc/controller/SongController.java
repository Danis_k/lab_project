package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.cc.controller.api.SongApi;
import ru.itis.cc.dto.song.NewOrUpdateSongDto;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.service.SongService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/songs")
public class SongController implements SongApi {

    private final SongService songService;

    @Override
    public ResponseEntity<SongsPage> getAllSongs(int page) {
        return ResponseEntity.ok(songService.getAllSongs(page));
    }

    @Override
    public ResponseEntity<SongDto> addSong(NewOrUpdateSongDto songDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(songService.addSong(songDto));
    }

    @Override
    public ResponseEntity<SongDto> getSong(Long id) {
        return ResponseEntity.ok(songService.getSong(id));
    }

    @Override
    public ResponseEntity<SongDto> updateSong(Long id, NewOrUpdateSongDto songDto) {
        return ResponseEntity.accepted().body(songService.updateSong(id, songDto));
    }

    @Override
    public ResponseEntity<?> deleteSong(Long id) {
        songService.deleteSong(id);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<SongDto> publishSong(Long id) {
        return ResponseEntity.accepted().body(songService.publishSong(id));
    }
}
