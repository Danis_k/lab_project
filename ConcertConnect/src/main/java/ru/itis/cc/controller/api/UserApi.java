package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.UpdateUserRequestDto;
import ru.itis.cc.dto.UserDto;

import javax.validation.Valid;

@Tags(
        value = {
                @Tag(name = "users")
        }
)
@RequestMapping("/users")
public interface UserApi {

    @Operation(summary = "get user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "user",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{user-id}")
    ResponseEntity<UserDto> getUserById(@Parameter(description = "user ID") @PathVariable("user-id") Long id);

    @Operation(summary = "get user by email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "user",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping
    ResponseEntity<UserDto> getUserByEmail(@Parameter(description = "email") @RequestParam String email);

    @Operation(summary = "update user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "updated user",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = UserDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "error information",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/{user-id}")
    ResponseEntity<UserDto> updateUser(@Parameter(description = "user ID") @PathVariable("user-id") Long id,
                                       @Valid @RequestBody UpdateUserRequestDto updateUserDto);
}
