package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.controller.api.ClientApi;
import ru.itis.cc.dto.ClientDto;
import ru.itis.cc.dto.RequestClientDto;
import ru.itis.cc.service.ClientService;

@RequiredArgsConstructor

@RestController
public class ClientController implements ClientApi {

    private final ClientService clientService;

    @Override
    public ResponseEntity<ClientDto> createClient(RequestClientDto clientDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(clientService.create(clientDto));
    }

    @Override
    public ResponseEntity<ClientDto> getClientById(@PathVariable Long clientId) {
        ClientDto client = clientService.getClientById(clientId);
        return ResponseEntity.ok(client);
    }

    @Override
    public ResponseEntity<ClientDto> updateClient(
            @PathVariable Long clientId,
            @RequestBody RequestClientDto clientDto
    ) {
        ClientDto updatedClient = clientService.updateClient(clientId, clientDto);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedClient);
    }
}
