package ru.itis.cc.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.OrganizerDto;
import ru.itis.cc.dto.RequestOrganizerDto;
import ru.itis.cc.service.OrganizerService;

@RequiredArgsConstructor
@RequestMapping("/organizers")
@RestController
public class OrganizerController {

    private final OrganizerService organizerService;


    @PostMapping("/create")
    public ResponseEntity<OrganizerDto> createOrganizer(@RequestBody RequestOrganizerDto organizerDto) {
        OrganizerDto createdOrganizer = organizerService.create(organizerDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdOrganizer);
    }

    @GetMapping("/{organizerId}")
    public ResponseEntity<OrganizerDto> getOrganizerById(@PathVariable Long organizerId) {
        OrganizerDto organizer = organizerService.getOrganizerById(organizerId);
        return ResponseEntity.ok(organizer);
    }

    @PutMapping("/update/{organizerId}")
    public ResponseEntity<OrganizerDto> updateOrganizer(
            @PathVariable Long organizerId,
            @RequestBody RequestOrganizerDto organizerDto
    ) {
        OrganizerDto updatedOrganizer = organizerService.updateOrganizer(organizerId, organizerDto);
        return ResponseEntity.ok(updatedOrganizer);
    }

}
