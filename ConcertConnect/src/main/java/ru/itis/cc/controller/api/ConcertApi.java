package ru.itis.cc.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.cc.dto.ExceptionDto;
import ru.itis.cc.dto.concert.ConcertDto;
import ru.itis.cc.dto.concert.ConcertsPage;
import ru.itis.cc.dto.concert.NewOrUpdateConcertDto;

@Tags(value = {
        @Tag(name = "concerts")
})
@RequestMapping("/concerts")
public interface ConcertApi {
    @Operation(summary = "Получение списка концертов")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с концертами",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ConcertsPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<ConcertsPage> getAllConcerts(@Parameter(description = "Номер страницы") @RequestParam("page") int page);

    @Operation(summary = "Добавление нового концерта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавление концерта",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ConcertDto.class))
                    })
    })
    @PostMapping
    ResponseEntity<ConcertDto> addConcert(@RequestBody NewOrUpdateConcertDto newOrUpdateConcertDto);

    @Operation(summary = "Получение концерта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с информацией о концерте",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ConcertDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })

    @GetMapping("/{concert-id}")
    ResponseEntity<ConcertDto> getConcert(@Parameter(description = "Идентификатор концерта", example = "1")
                                          @PathVariable("concert-id") Long id);


    @Operation(summary = "Обновление концерта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Обновленный урок",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ConcertDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{concert-id}")
    ResponseEntity<ConcertDto> updateConcert(
            @Parameter(description = "Идентификатор концерта", example = "1") @PathVariable("concert-id") Long id,
            @RequestBody NewOrUpdateConcertDto updatedConcert);


    @Operation(summary = "Удаление концерта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Концерт удален"),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @DeleteMapping ("/{concert-id}")
    ResponseEntity<?> deleteConcert(
            @Parameter(description = "Идентификатор концерта", example = "1") @PathVariable("concert-id") Long id);


    @Operation(summary = "Публикация концерта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Опубликованный урок",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ConcertDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Сведение об ошибке",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionDto.class))
                    })
    })
    @PutMapping("/{concert-id}/publish")
    ResponseEntity<ConcertDto> publishConcert(
            @Parameter(description = "Идентификатор концерта", example = "1") @PathVariable("concert-id") Long id);

}
