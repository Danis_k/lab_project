package ru.itis.cc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Song;

public interface SongRepository extends JpaRepository<Song, Long> {
    Page<Song> findAllByStateOrderById(Pageable pageable, Song.State state);
}
