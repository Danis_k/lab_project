package ru.itis.cc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Singer;

public interface SingerRepository extends JpaRepository<Singer, Long> {
    Page<Singer> findAllByStateOrderById(Pageable pageable, Singer.State state);
}
