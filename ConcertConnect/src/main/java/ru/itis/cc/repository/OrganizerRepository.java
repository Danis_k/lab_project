package ru.itis.cc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.cc.model.Organizer;

public interface OrganizerRepository extends JpaRepository<Organizer, Long> {
}
