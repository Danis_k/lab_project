package ru.itis.cc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.cc.model.Song;
import ru.itis.cc.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Page<Ticket> findAllByStateOrderById(Pageable pageable, Ticket.State state);
}


