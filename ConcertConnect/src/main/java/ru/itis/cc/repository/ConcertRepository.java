package ru.itis.cc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.cc.model.Concert;

public interface ConcertRepository extends JpaRepository<Concert, Long> {
    Page<Concert> findAllByStateOrderById(Pageable pageable, Concert.State state);
}
