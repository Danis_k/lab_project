package ru.itis.cc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.cc.model.Client;


public interface ClientRepository extends JpaRepository<Client, Long> {
}
