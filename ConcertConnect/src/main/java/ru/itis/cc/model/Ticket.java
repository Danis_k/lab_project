package ru.itis.cc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String location;

    private String place;
    @OneToOne
    private Client client;
    @ManyToOne
    private Concert concert;

    public enum State{
        ACTIVE,
        DRAFT,
        DELETED
    }
    @Enumerated(value = EnumType.STRING)
    private Ticket.State state;
}
