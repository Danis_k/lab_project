package ru.itis.cc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Concert {

    public enum State{
        ACTIVE,
        DRAFT,
        DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String city;
    private Date data;
    private String place;

    @ManyToOne
    @JoinColumn(name = "singer_id")
    private Singer singer;

    //ToDo: добавить потом связь
//    @ManyToOne
//    @JoinColumn(name = "organizer_id")
//    private Organizer organizer;

    @Enumerated(value = EnumType.STRING)
    private State state;

}
