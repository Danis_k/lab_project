package ru.itis.cc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Song {

    public enum State{
        ACTIVE,
        DRAFT,
        DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_of_song")
    private String nameOfSong;

    @Column(name = "text_of_song")
    private String textOfSong;

    private String genre;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToMany(mappedBy = "songs")
    List<Singer> singers;
}
