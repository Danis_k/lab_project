package ru.itis.cc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String textOfMessage;
    @ManyToOne(optional = false)
    private Client client;
    @ManyToOne(optional = false)
    private Chat chat;

    public enum State{
        ACTIVE,
        DRAFT,
        DELETED
    }
    @Enumerated(value = EnumType.STRING)
    private Message.State state;

}

