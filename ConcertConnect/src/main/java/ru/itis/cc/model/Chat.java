package ru.itis.cc.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Concert concert;

    public enum State{
        ACTIVE,
        DRAFT,
        DELETED
    }
    @Enumerated(value = EnumType.STRING)
    private Chat.State state;

}
