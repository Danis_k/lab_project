package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.singer.NewOrUpdateSingerDto;
import ru.itis.cc.dto.singer.SingerDto;
import ru.itis.cc.dto.singer.SingersPage;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.exception.NotFoundException;
import ru.itis.cc.model.Singer;
import ru.itis.cc.model.Song;
import ru.itis.cc.repository.SingerRepository;
import ru.itis.cc.service.SingerService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SingerServiceImpl implements SingerService {

    private final SingerRepository singerRepository;

    @Value("${default.page.size}")
    private int defaultPageSize;

    private Singer getSingerOrThrow(Long id) {
        return singerRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Исполнитель с идентификатором" + id + "не найден"));
    }


    @Override
    public SingersPage getAllSingers(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

        Page<Singer> singersPage = singerRepository.findAllByStateOrderById(pageRequest, Singer.State.ACTIVE);

        return SingersPage.builder()
                .singers(SingerDto.from(singersPage.getContent()))
                .totalPagesCount(singersPage.getTotalPages())
                .build();
    }

    @Override
    public SingerDto addSinger(NewOrUpdateSingerDto singerDto) {

        //Todo: добавить потом связь с организатором

        Singer singer = Singer.builder()
                .firstName(singerDto.getFirstName())
                .lastName(singerDto.getLastName())
                .socialMedia(singerDto.getLastName())
                .build();

        singerRepository.save(singer);

        return SingerDto.from(singer);
    }

    @Override
    public SingerDto getSinger(Long id) {
        Singer singer = getSingerOrThrow(id);
        return SingerDto.from(singer);
    }

    @Override
    public SingerDto updateSinger(Long id, NewOrUpdateSingerDto singerDto) {

        Singer singerForUpdate = getSingerOrThrow(id);

        singerForUpdate.setFirstName(singerDto.getFirstName());
        singerForUpdate.setLastName(singerDto.getLastName());
        singerForUpdate.setSocialMedia(singerDto.getSocialMedia());

        singerRepository.save(singerForUpdate);

        return SingerDto.from(singerForUpdate);
    }

    @Override
    public void deleteSinger(Long id) {
        Singer singerForDelete = getSingerOrThrow(id);
        singerForDelete.setState(Singer.State.DELETED);

        singerRepository.save(singerForDelete);
    }

    @Override
    public SingerDto publishSinger(Long id) {

        Singer singerForPublish = getSingerOrThrow(id);

        singerForPublish.setState(Singer.State.ACTIVE);
        singerRepository.save(singerForPublish);

        return SingerDto.from(singerForPublish);
    }

    @Override
    public SongsPage getAllSongBySinger(int page, Long id) {

        Singer singer = getSingerOrThrow(id);

        List<Song> songs = singer.getSongs();

        int totalPages = (int) Math.ceil((double) songs.size() / defaultPageSize);

        return SongsPage.builder()
                .songs(SongDto.from(songs))
                .totalPagesCount(totalPages)
                .build();

    }
}
