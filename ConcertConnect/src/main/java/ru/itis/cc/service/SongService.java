package ru.itis.cc.service;

import ru.itis.cc.dto.song.NewOrUpdateSongDto;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;

public interface SongService {
    SongsPage getAllSongs(int page);

    SongDto addSong(NewOrUpdateSongDto songDto);

    SongDto getSong(Long id);

    SongDto updateSong(Long id, NewOrUpdateSongDto songDto);

    void deleteSong(Long id);

    SongDto publishSong(Long id);
}
