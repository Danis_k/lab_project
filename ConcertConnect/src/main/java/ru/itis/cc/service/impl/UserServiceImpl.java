package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.CreateUserRequestDto;
import ru.itis.cc.dto.UpdateUserRequestDto;
import ru.itis.cc.dto.UserDto;
import ru.itis.cc.dto.UserResponseDto;
import ru.itis.cc.model.User;
import ru.itis.cc.repository.UserRepository;
import ru.itis.cc.service.UserService;

import static ru.itis.cc.dto.UserDto.fromEntity;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder encoder;

    @Override
    public UserDto getUserById(Long id) {
        User user = getUserOrThrow(id);

        return fromEntity(user);
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User with email: " + email + "not found"));

        return fromEntity(user);
    }

    @Override
    public UserResponseDto create(CreateUserRequestDto createUserDto) {
        String encodedPassword = encoder.encode(createUserDto.getPassword());
        User user = User.builder()
                .email(createUserDto.getEmail())
                .password(encodedPassword)
                .state(User.State.NOT_CONFIRMED)
                .build();

        userRepository.save(user);

        return UserResponseDto.from(user);
    }

    @Override
    public void delete(Long userId) {
        User user = getUserOrThrow(userId);

        user.setState(User.State.DELETE);
        userRepository.save(user);
    }

    @Override
    public UserDto updateUser(Long userId, UpdateUserRequestDto updateUser) {
        User user = getUserOrThrow(userId);

        user.setEmail(updateUser.getEmail());
        user.setPassword(encoder.encode(updateUser.getEmail()));

        userRepository.save(user);

        return fromEntity(user);
    }

    private User getUserOrThrow(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User with id: " + id + "not found"));
    }
}
