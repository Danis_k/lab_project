package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.ClientDto;
import ru.itis.cc.dto.RequestClientDto;
import ru.itis.cc.exception.NotFoundException;
import ru.itis.cc.model.Client;
import ru.itis.cc.repository.ClientRepository;
import ru.itis.cc.repository.UserRepository;
import ru.itis.cc.service.ClientService;

import static ru.itis.cc.dto.ClientDto.fromEntity;

@RequiredArgsConstructor
@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    private final UserRepository userRepository;

    //    TODO: взять текущего юсера через секбюрати
    @Override
    public ClientDto create(RequestClientDto clientDto) {
        Client client = Client.builder()
                .user(userRepository.getById(1L))
                .firstName(clientDto.getFirstName())
                .lastName(clientDto.getLastName())
                .phoneNumber(clientDto.getPhoneNumber())
                .build();

        clientRepository.save(client);

        return fromEntity(client);
    }

    @Override
    public ClientDto getClientById(Long clientId) {
        Client client = getClientOrThrow(clientId);

        return fromEntity(client);
    }

    @Override
    public ClientDto updateClient(Long updateClientId, RequestClientDto client) {
        Client updateClient = getClientOrThrow(updateClientId);

        updateClient.setFirstName(client.getFirstName());
        updateClient.setLastName(client.getLastName());
        updateClient.setPhoneNumber(client.getPhoneNumber());

        clientRepository.save(updateClient);

        return fromEntity(updateClient);
    }

    private Client getClientOrThrow(Long clientId) {
        return clientRepository.findById(clientId)
                .orElseThrow(() -> new NotFoundException("Client with id:" + clientId + "not found"));
    }
}
