package ru.itis.cc.service;

import ru.itis.cc.dto.singer.NewOrUpdateSingerDto;
import ru.itis.cc.dto.singer.SingerDto;
import ru.itis.cc.dto.singer.SingersPage;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.model.Singer;

public interface SingerService {
    SingersPage getAllSingers(int page);

    SingerDto addSinger(NewOrUpdateSingerDto singerDto);

    SingerDto getSinger(Long id);

    SingerDto updateSinger(Long id, NewOrUpdateSingerDto singerDto);

    void deleteSinger(Long id);

    SingerDto publishSinger(Long id);

    SongsPage getAllSongBySinger(int page, Long id);
}
