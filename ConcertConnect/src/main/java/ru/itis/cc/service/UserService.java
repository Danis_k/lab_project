package ru.itis.cc.service;


import ru.itis.cc.dto.CreateUserRequestDto;
import ru.itis.cc.dto.UpdateUserRequestDto;
import ru.itis.cc.dto.UserDto;
import ru.itis.cc.dto.UserResponseDto;

public interface UserService {
    UserDto getUserById(Long id);

    UserDto getUserByEmail(String email);

    UserResponseDto create(CreateUserRequestDto createUserDto);

    void delete(Long userId);

    UserDto updateUser(Long userId, UpdateUserRequestDto updateUser);
}
