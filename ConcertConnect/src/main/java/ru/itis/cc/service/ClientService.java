package ru.itis.cc.service;

import ru.itis.cc.dto.ClientDto;
import ru.itis.cc.dto.RequestClientDto;

public interface ClientService {
    ClientDto create(RequestClientDto client);

    ClientDto getClientById(Long clientId);

    ClientDto updateClient(Long updateClientId, RequestClientDto client);
}
