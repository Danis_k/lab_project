package ru.itis.cc.service;

import ru.itis.cc.dto.FeedbackDto;
import ru.itis.cc.dto.FeedbackPage;
import ru.itis.cc.dto.RequestFeedbackDto;

public interface FeedbackService {

    FeedbackPage getAllFeedback(int page);

    FeedbackDto addFeedback(RequestFeedbackDto feedbackDto);

    FeedbackDto getFeedback(Long feedbackId);

    FeedbackDto updateFeedback(Long feedbackId, RequestFeedbackDto feedbackDto);

    void delete(Long feedbackId);

    FeedbackDto publishFeedback(Long feedbackId);
}
