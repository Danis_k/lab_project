package ru.itis.cc.service;

import ru.itis.cc.dto.OrganizerDto;
import ru.itis.cc.dto.RequestOrganizerDto;

public interface OrganizerService {
    OrganizerDto create(RequestOrganizerDto organizer);

    OrganizerDto getOrganizerById(Long organizerId);

    OrganizerDto updateOrganizer(Long id, RequestOrganizerDto organizer);
}
