package ru.itis.cc.service;

import ru.itis.cc.dto.concert.ConcertDto;
import ru.itis.cc.dto.concert.ConcertsPage;
import ru.itis.cc.dto.concert.NewOrUpdateConcertDto;

public interface ConcertService {
    ConcertsPage getAllConcerts(int page);

    ConcertDto addConcert(NewOrUpdateConcertDto newOrUpdateConcertDto);

    ConcertDto getConcert(Long id);

    ConcertDto updateConcert(Long id, NewOrUpdateConcertDto updatedConcert);

    void deleteConcert(Long id);

    ConcertDto publishConcert(Long id);
}
