package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.concert.ConcertDto;
import ru.itis.cc.dto.concert.ConcertsPage;
import ru.itis.cc.dto.concert.NewOrUpdateConcertDto;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Singer;
import ru.itis.cc.repository.ConcertRepository;
import ru.itis.cc.repository.SingerRepository;
import ru.itis.cc.service.ConcertService;
import ru.itis.cc.exception.NotFoundException;

@RequiredArgsConstructor
@Service
public class ConcertServiceImpl implements ConcertService {
    private final ConcertRepository concertRepository;

    private final SingerRepository singerRepository;

    @Value("${default.page.size}")
    private int defaultPageSize;

    private Concert getConcertOrThrow(Long id) {
        return concertRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Концерт с идентификатором" + id + "не найден"));
    }

    @Override
    public ConcertsPage getAllConcerts(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

        Page<Concert> concertsPage = concertRepository.findAllByStateOrderById(pageRequest, Concert.State.ACTIVE);

        return ConcertsPage.builder()
                .concerts(ConcertDto.from(concertsPage.getContent()))
                .totalPagesCount(concertsPage.getTotalPages())
                .build();

    }

    @Override
    public ConcertDto addConcert(NewOrUpdateConcertDto newOrUpdateConcertDto) {

        Singer singer = singerRepository.findById(newOrUpdateConcertDto.getSingerID())
                .orElseThrow(() -> new NotFoundException("Исполнитель с идентификатором" + newOrUpdateConcertDto.getSingerID() + "не найден"));

        Concert concert = Concert.builder()
                .data(newOrUpdateConcertDto.getData())
                .place(newOrUpdateConcertDto.getPlace())
                .singer(singer)
                .build();

        concertRepository.save(concert);

        return ConcertDto.from(concert);
    }

    @Override
    public ConcertDto getConcert(Long id) {
        Concert concert = getConcertOrThrow(id);
        return ConcertDto.from(concert);
    }

    @Override
    public ConcertDto updateConcert(Long id, NewOrUpdateConcertDto updatedConcert) {

        Concert concertForUpdate = getConcertOrThrow(id);

        concertForUpdate.setData(updatedConcert.getData());
        concertForUpdate.setPlace(updatedConcert.getPlace());
        concertForUpdate.setCity(updatedConcert.getCity());

        if (updatedConcert.getSingerID() != null){
            Singer singer = singerRepository.findById(updatedConcert.getSingerID())
                    .orElseThrow(() -> new NotFoundException("Исполнитель с идентификатором" + updatedConcert.getSingerID() + "не найден"));
            concertForUpdate.setSinger(singer);
        }

        concertRepository.save(concertForUpdate);

        return ConcertDto.from(concertForUpdate);
    }

    @Override
    public void deleteConcert(Long id) {
        Concert concertForDelete = getConcertOrThrow(id);

        concertForDelete.setState(Concert.State.DELETED);
        concertRepository.save(concertForDelete);
    }

    @Override
    public ConcertDto publishConcert(Long id) {
        Concert concertForPublish = getConcertOrThrow(id);

        concertForPublish.setState(Concert.State.ACTIVE);
        concertRepository.save(concertForPublish);

        return ConcertDto.from(concertForPublish);
    }
}
