package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.dto.ticket.TicketDto;
import ru.itis.cc.dto.ticket.TicketsPage;
import ru.itis.cc.model.Client;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Song;
import ru.itis.cc.model.Ticket;
import ru.itis.cc.repository.ClientRepository;
import ru.itis.cc.repository.ConcertRepository;
import ru.itis.cc.repository.TicketRepository;
import ru.itis.cc.service.TicketService;

import java.util.Optional;

import static ru.itis.cc.dto.ticket.TicketDto.from;

@RequiredArgsConstructor
@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final ConcertRepository concertRepository;
    private final ClientRepository clientRepository;

    public TicketDto getTicketById(Long id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        return ticketOptional.map(TicketDto::from).orElse(null);
    }

    public TicketDto createTicket(TicketDto ticketDTO) {
        Concert concert = concertRepository.findById(ticketDTO.getConcertId()).orElse(null);
        Client client = clientRepository.findById(ticketDTO.getClientId()).orElse(null);
        Ticket ticket = Ticket.builder()
                .concert(concert)
                .client(client)
                .place(ticketDTO.getPlace())
                .location(ticketDTO.getLocation())
                .build();
        return from(ticketRepository.save(ticket));
    }

    public TicketDto updateTicket(Long id, TicketDto ticketDTO) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        if (ticketOptional.isPresent()) {
            Concert concert = concertRepository.findById(ticketDTO.getConcertId()).orElse(null);
            Client client = clientRepository.findById(ticketDTO.getClientId()).orElse(null);

            Ticket ticket = ticketOptional.get();

            ticket.setId(ticketDTO.getId());
            ticket.setConcert(concert);
            ticket.setClient(client);
            ticket.setPlace(ticketDTO.getPlace());
            ticket.setLocation(ticketDTO.getLocation());
            return from(ticketRepository.save(ticket));
        } else {
            return null;
        }
    }

    public boolean deleteTicket(Long id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        if (ticketOptional.isPresent()) {
            ticketRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
    @Value("${default.page.size}")
    private int defaultPageSize;
    @Override
    public TicketsPage getAllTickets(int page) {
            PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

            Page<Ticket> ticketsPage = ticketRepository.findAllByStateOrderById(pageRequest, Ticket.State.ACTIVE);

            return TicketsPage.builder()
                    .tickets(TicketDto.from(ticketsPage.getContent()))
                    .totalPagesCount(ticketsPage.getTotalPages())
                    .build();
        }
    }
