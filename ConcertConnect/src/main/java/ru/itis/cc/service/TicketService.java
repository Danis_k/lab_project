package ru.itis.cc.service;

import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.ticket.TicketDto;
import ru.itis.cc.dto.ticket.TicketsPage;
import ru.itis.cc.service.impl.TicketServiceImpl;

public interface TicketService {
    TicketDto getTicketById(Long id);
    TicketDto createTicket(TicketDto ticketDTO);
    TicketDto updateTicket(Long id, TicketDto ticketDTO);
    boolean deleteTicket(Long id);
    TicketsPage getAllTickets(int page);
}
