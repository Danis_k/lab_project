package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.OrganizerDto;
import ru.itis.cc.dto.RequestOrganizerDto;
import ru.itis.cc.exception.NotFoundException;
import ru.itis.cc.model.Organizer;
import ru.itis.cc.repository.OrganizerRepository;
import ru.itis.cc.service.OrganizerService;

import static ru.itis.cc.dto.OrganizerDto.fromEntity;

@RequiredArgsConstructor
@Service
public class OrganizerServiceImpl implements OrganizerService {

    private final OrganizerRepository organizerRepository;

    //    TODO: взять текущего юсера через секбюрати
    @Override
    public OrganizerDto create(RequestOrganizerDto organizer) {
        Organizer newOrganizer = Organizer.builder()
//                .user()
                .nameCompany(organizer.getNameCompany())
                .build();

        organizerRepository.save(newOrganizer);

        return fromEntity(newOrganizer);
    }

    @Override
    public OrganizerDto getOrganizerById(Long organizerId) {
        Organizer organizer = getOrganizerOrThrow(organizerId);

        return fromEntity(organizer);
    }

    @Override
    public OrganizerDto updateOrganizer(Long id, RequestOrganizerDto requestOrganizer) {
        Organizer newOrganization = getOrganizerOrThrow(id);

        newOrganization.setNameCompany(requestOrganizer.getNameCompany());

        return fromEntity(newOrganization);
    }

    private Organizer getOrganizerOrThrow(Long organizerId) {
        return organizerRepository.findById(organizerId)
                .orElseThrow(() -> new NotFoundException("Organizer with id:" + organizerId + "not found"));
    }
}
