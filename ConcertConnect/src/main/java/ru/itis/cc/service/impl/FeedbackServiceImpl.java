package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.FeedbackDto;
import ru.itis.cc.dto.FeedbackPage;
import ru.itis.cc.dto.RequestFeedbackDto;
import ru.itis.cc.exception.NotFoundException;
import ru.itis.cc.model.Client;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Feedback;
import ru.itis.cc.repository.ClientRepository;
import ru.itis.cc.repository.ConcertRepository;
import ru.itis.cc.repository.FeedbackRepository;
import ru.itis.cc.service.FeedbackService;


import static ru.itis.cc.dto.FeedbackDto.from;

@RequiredArgsConstructor
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    private final ClientRepository clientRepository;
    private final ConcertRepository concertRepository;
    private final int defaultPageSize = 5;

    @Override
    public FeedbackPage getAllFeedback(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);
        Page<Feedback> feedbacksPage = feedbackRepository.findAllByStateOrderById(Feedback.State.ACTIVE, pageRequest);
        return FeedbackPage.builder()
                .feedbacks(from(feedbacksPage.getContent()))
                .totalPagesCount(feedbacksPage.getTotalPages())
                .build();
    }

    @Override
    public FeedbackDto addFeedback(RequestFeedbackDto feedbackDto) {
        Client client = getClientOrThrow(feedbackDto.getClientId());
        Concert concert = getConcertOrThrow(feedbackDto.getConcertId());

        Feedback feedback = Feedback.builder()
                .feedbackText(feedbackDto.getFeedbackText())
                .client(client)
                .concert(concert)
                .name(feedbackDto.getName())
                .state(Feedback.State.DRAFT)
                .build();
        feedbackRepository.save(feedback);

        return from(feedback);
    }


    @Override
    public FeedbackDto getFeedback(Long feedbackId) {
        Feedback feedback = getFeedbackOrElseThrow(feedbackId);
        return from(feedback);
    }


    @Override
    public FeedbackDto updateFeedback(Long feedbackId, RequestFeedbackDto feedbackDto) {
        Feedback feedbackForUpdate = getFeedbackOrElseThrow(feedbackId);
        Client client = getClientOrThrow(feedbackDto.getClientId());
        Concert concert = getConcertOrThrow(feedbackDto.getConcertId());

        feedbackForUpdate.setFeedbackText(feedbackDto.getFeedbackText());
        feedbackForUpdate.setClient(client);
        feedbackForUpdate.setConcert(concert);

        feedbackRepository.save(feedbackForUpdate);

        return from(feedbackForUpdate);
    }

    @Override
    public void delete(Long feedbackId) {
        Feedback feedbackForDelete = getFeedbackOrElseThrow(feedbackId);

        feedbackForDelete.setState(Feedback.State.DELETED);
        feedbackRepository.save(feedbackForDelete);
    }

    @Override
    public FeedbackDto publishFeedback(Long feedbackId) {
        Feedback feedbackForPublish = getFeedbackOrElseThrow(feedbackId);

        feedbackForPublish.setState(Feedback.State.ACTIVE);
        feedbackRepository.save(feedbackForPublish);

        return from(feedbackForPublish);
    }


    private Feedback getFeedbackOrElseThrow(Long feedbackId) {
        return feedbackRepository.findById(feedbackId)
                .orElseThrow(() -> new NotFoundException("Feedback with id <" + feedbackId + "> not found"));
    }

    private Client getClientOrThrow(Long id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Client with id <" + id + "> not found"));
    }

    private Concert getConcertOrThrow(Long id) {
        return concertRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Concert with id <" + id + "> not found"));
    }
}
