package ru.itis.cc.service;

import ru.itis.cc.dto.message.MessageDto;
import ru.itis.cc.dto.message.MessagesPage;

public interface MessageService {
    MessageDto getMessageById(Long id);
    MessageDto createMessage(MessageDto messageDTO);
    MessageDto updateMessage(Long id, MessageDto messageDTO);
    boolean deleteMessage(Long id);
  MessagesPage getAllMessages(int page);
}
