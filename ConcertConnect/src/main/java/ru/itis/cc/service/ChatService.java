package ru.itis.cc.service;


import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.chat.ChatsPage;

public interface ChatService {
    ChatDto getChatById(Long id);
    ChatDto createChat(ChatDto chatDto);
    ChatDto updateChat(Long id, ChatDto chatDTO);
    boolean deleteChat(Long id);
    ChatsPage getAllChats(int page);
}
