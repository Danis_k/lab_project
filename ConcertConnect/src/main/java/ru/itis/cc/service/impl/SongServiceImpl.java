package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.song.NewOrUpdateSongDto;
import ru.itis.cc.dto.song.SongDto;
import ru.itis.cc.dto.song.SongsPage;
import ru.itis.cc.exception.NotFoundException;
import ru.itis.cc.model.Singer;
import ru.itis.cc.model.Song;
import ru.itis.cc.repository.SingerRepository;
import ru.itis.cc.repository.SongRepository;
import ru.itis.cc.service.SongService;

import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class SongServiceImpl implements SongService {

    private final SongRepository songRepository;

    private final SingerRepository singerRepository;

    @Value("${default.page.size}")
    private int defaultPageSize;

    private Song getSongOrThrow(Long id) {
        return songRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Песня с идентификатором" + id + "не найден"));
    }

    @Override
    public SongsPage getAllSongs(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

        Page<Song> songsPage = songRepository.findAllByStateOrderById(pageRequest, Song.State.ACTIVE);

        return SongsPage.builder()
                .songs(SongDto.from(songsPage.getContent()))
                .totalPagesCount(songsPage.getTotalPages())
                .build();
    }

    @Override
    public SongDto addSong(NewOrUpdateSongDto songDto) {
        Song song = Song.builder()
                .nameOfSong(songDto.getNameOfSong())
                .textOfSong(songDto.getTextOfSong())
                .genre(songDto.getGenre())
                .build();

        List<Singer> singers = singerRepository.findAllById(songDto.getSingersId());

        // Todo: пофиксить, чтобы была нормально связь с исполнителями
        song.setSingers(singers);

        songRepository.save(song);

        return SongDto.from(song);

    }

    @Override
    public SongDto getSong(Long id) {
        Song song = getSongOrThrow(id);
        return SongDto.from(song);
    }

    @Override
    public SongDto updateSong(Long id, NewOrUpdateSongDto songDto) {

        Song songForUpdate = getSongOrThrow(id);

        songForUpdate.setNameOfSong(songDto.getNameOfSong());
        songForUpdate.setTextOfSong(songDto.getTextOfSong());
        songForUpdate.setGenre(songDto.getGenre());

        if(songDto.getSingersId() != null){
            List<Singer> singers = singerRepository.findAllById(songDto.getSingersId());
            singers.forEach(singer -> songForUpdate.getSingers().add(singer));
        }

        songRepository.save(songForUpdate);

        return SongDto.from(songForUpdate);
    }

    @Override
    public void deleteSong(Long id) {
        Song songForDelete = getSongOrThrow(id);
        songForDelete.setState(Song.State.DELETED);

        songRepository.save(songForDelete);
    }

    @Override
    public SongDto publishSong(Long id) {

        Song songForPublish = getSongOrThrow(id);

        songForPublish.setState(Song.State.ACTIVE);
        songRepository.save(songForPublish);

        return SongDto.from(songForPublish);
    }
}
