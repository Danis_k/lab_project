package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.chat.ChatsPage;
import ru.itis.cc.dto.message.MessageDto;
import ru.itis.cc.dto.message.MessagesPage;
import ru.itis.cc.model.Chat;
import ru.itis.cc.model.Client;
import ru.itis.cc.model.Message;
import ru.itis.cc.repository.ChatRepository;
import ru.itis.cc.repository.ClientRepository;
import ru.itis.cc.repository.MessageRepository;
import ru.itis.cc.service.MessageService;

import java.util.Optional;

import static ru.itis.cc.dto.message.MessageDto.from;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final ClientRepository clientRepository;
    private final ChatRepository chatRepository;
    

    public MessageDto getMessageById(Long id) {
        Optional<Message> messageOptional = messageRepository.findById(id);
        return messageOptional.map(MessageDto::from).orElse(null);
    }

    public MessageDto createMessage(MessageDto messageDTO) {
        Client client = clientRepository.findById(messageDTO.getClientId()).orElse(null);
        Chat chat = chatRepository.findById(messageDTO.getChatId()).orElse(null);
        Message message = Message.builder()
                .client(client)
                .chat(chat)
                .textOfMessage(messageDTO.getTextOfMessage())
                .build();

        return from( messageRepository.save(message));
    }

    public MessageDto updateMessage(Long id, MessageDto messageDTO) {
        Optional<Message> messageOptional = messageRepository.findById(id);
        if (messageOptional.isPresent()) {
            Message message = messageOptional.get();
            Client client = clientRepository.findById(messageDTO.getClientId()).orElse(null);
            Chat chat = chatRepository.findById(messageDTO.getChatId()).orElse(null);

            message.setId(messageDTO.getId());
            message.setTextOfMessage(messageDTO.getTextOfMessage());
            message.setClient(client);
            message.setChat(chat);
            return from(messageRepository.save(message));
        } else {
            return null;
        }
    }

    public boolean deleteMessage(Long id) {
        Optional<Message> messageOptional = messageRepository.findById(id);
        if (messageOptional.isPresent()) {
            messageRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
    @Value("${default.page.size}")
    private int defaultPageSize;
    @Override
    public MessagesPage getAllMessages(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

        Page<Message> messagesPage = messageRepository.findAllByStateOrderById(pageRequest, Message.State.ACTIVE);

        return MessagesPage.builder()
                .messages(MessageDto.from(messagesPage.getContent()))
                .totalPagesCount(messagesPage.getTotalPages())
                .build();
    }
}
