package ru.itis.cc.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.cc.dto.chat.ChatDto;
import ru.itis.cc.dto.chat.ChatsPage;
import ru.itis.cc.dto.ticket.TicketDto;
import ru.itis.cc.dto.ticket.TicketsPage;
import ru.itis.cc.model.Chat;
import ru.itis.cc.model.Concert;
import ru.itis.cc.model.Ticket;
import ru.itis.cc.repository.ChatRepository;
import ru.itis.cc.repository.ConcertRepository;
import ru.itis.cc.service.ChatService;

import java.util.Optional;

import static ru.itis.cc.dto.chat.ChatDto.from;

@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {
    private final ChatRepository chatRepository;
    private final ConcertRepository concertRepository;


    public ChatDto getChatById(Long id) {
        Optional<Chat> chat = chatRepository.findById(id);
        return chat.map(ChatDto::from).orElse(null);
    }

    public ChatDto createChat(ChatDto chatDTO) {
        Concert concert = concertRepository.findById(chatDTO.getConcertId()).orElse(null);
        Chat chat = Chat.builder()
                .concert(concert)
                .build();

        return from(chatRepository.save(chat));
    }


    public ChatDto updateChat(Long id, ChatDto chatDTO) {
            Optional<Chat> chatOptional = chatRepository.findById(id);
            if (chatOptional.isPresent()) {
                Chat chat = chatOptional.get();
                Concert concert = concertRepository.findById(chatDTO.getConcertId()).orElse(null);

                chat.setId(chatDTO.getId());
                chat.setConcert(concert);
                return from(chatRepository.save(chat));
            } else {
                return null;
            }
        }

    public boolean deleteChat(Long id) {
        Optional<Chat> chatOptional = chatRepository.findById(id);
        if (chatOptional.isPresent()) {
            chatRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
    @Value("${default.page.size}")
    private int defaultPageSize;
    @Override
    public ChatsPage getAllChats(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);

        Page<Chat> chatsPage = chatRepository.findAllByStateOrderById(pageRequest, Chat.State.ACTIVE);

        return ChatsPage.builder()
                .chats(ChatDto.from(chatsPage.getContent()))
                .totalPagesCount(chatsPage.getTotalPages())
                .build();
    }
}